//
//  WBStatus.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/22.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit
import YYModel

//微博数据模型
class WBStatus: NSObject {
    
    //Int 类型,在64位的机器是64位
    //如果不写在 iPad2/iPhone5/5c/4s/4 会数据溢出
    var id: Int64 = 0
    var text: String?
    
    //重写 description 的计算型属性
    override var description: String{
        return yy_modelDescription()
    }
}
