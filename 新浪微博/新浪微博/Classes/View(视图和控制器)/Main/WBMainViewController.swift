//
//  WBMainViewController.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/16.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit
import SVProgressHUD

///主控制器
class WBMainViewController: UITabBarController {
    
    //定时器
    var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupChildControllers()
        
        setupComposeBtn()
        
        setupTimer()
        
        //设置新特性视图
        //setupNewFeatureView()

        //设置代理
        delegate = self
        
        //注册通知
        NotificationCenter.default.addObserver(self, selector: #selector(userLogin), name: NSNotification.Name(rawValue: WBUserShouldLoginNotification), object: nil)
    }
    
    deinit {
        //销毁时钟
        timer?.invalidate()
        
        //注销通知
        NotificationCenter.default.removeObserver(self)
    }
    
    /**
     portrait    : 竖屏，肖像
     landscape   : 横屏，风景画
     
     - 使用代码控制设备的方向，好处，可以在在需要横屏的时候，单独处理！
     - 设置支持的方向之后，当前的控制器及子控制器都会遵守这个方向！
     - 如果播放视频，通常是通过 modal 展现的！
     */
    //设置所有为竖屏.横屏在 DemoController
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - 监听方法
    ///FIXME: 没有实现 (private 保护{})
    @objc func composeStatus(){
        print("撰写微博")
        //测试方向旋转
        let vc = UIViewController()
        vc.view.backgroundColor = UIColor.cz_random()
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
        
    }
    
    //MARK: - 私有空间不能定义属性
    //@objc 允许这个函数在运行时通过 OC 的消息机制 被调用
    @objc lazy var composeBtn: UIButton = UIButton.cz_imageButton(
        "tabbar_compose_icon_add",
        backgroundImageName: "tabbar_compose_button")

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: - 新特性视图处理
extension WBMainViewController {
    
    //设置新特性视图
    func setupNewFeatureView() -> () {
        
        //0.判断是否登录
        if !WBNetworkManager.shared.userLogon {
            return
        }
        
        //1.检查版本是否更新
        
        //2.如果更新,显示新特性,否则显示欢迎
        let v = isNewVersion ? WBNewFeature() : WBWelcomeView().welcomView()
        //v.frame = view.bounds
        
        //3.添加视图
        view.addSubview(v)
    }
    
    //extension 可以有计算型属性,不会占用存储空间
    //构造函数,给属性分配空间
    /**
     版本号
     - 在 AppStore 每次升级应用程序,版本号都需要增加
     - 组成 主版本号,次版本号,修订版本号
     - 主版本号,意味着大的修改,使用者也需要做大的适应
     - 次版本号, 意味着小的修改,某些函数和方法的使用或者参数有变化
     - 修订版本号, 框架/程序 内部 bug 的修订,不会对使用者造成任何的影响
    */
    private var isNewVersion: Bool {
        
        //1.取当前的版本号
        let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        
        //2.取保存在 'Document(iTunes 备份)[最理想保存在用户偏好]' 目录中的之前的版本号
        let path: String = ("version" as NSString).cz_appendDocumentDir()
        let sandBoxVersion = try? String(contentsOfFile: path)
        
        //3.将当前版本号保存在沙盒 1.0.1
        try? currentVersion.write(toFile: path, atomically: true, encoding: .utf8)
        
        //4.返回两个版本号'是否一致' new
        return currentVersion != sandBoxVersion
    }
}

extension WBMainViewController : UITabBarControllerDelegate{
    
    // - 将要选择 TabBarItem
    // - 目标控制器

    /**
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        //1.获取控制器在数组中的索引
        let idx = (childViewControllers as NSArray).index(of: viewController)
        //2.判断当前索引 是首页,同时 idx 也是首页,重复点击首页的按钮
        if selectedIndex == 0 && idx == selectedIndex {
            print("点击首页")
            
            //3 让表格滚动到底部
                //a 获取到控制器
                let nav = childViewControllers[0] as! UINavigationController
                let vc = nav.childViewControllers[0] as! WBHomeViewController
            
                //b 滚动至顶部
                vc.tableView?.setContentOffset(CGPoint(x: 0,y: -64), animated: true)
            
            //4 刷新表格 增加延迟,是保证表格先滚动到顶部再刷新
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                
                vc.loadData()
            })
            
        }
        
        //判断是 目标控制器 否是 UIViewController
        return viewController.isMember(of: UIViewController.self)
    }
 */

}

extension WBMainViewController{
    
    //定义时钟
    func setupTimer(){
        timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer(){
        
        if !WBNetworkManager.shared.userLogon {
            return
        }
        
        let count = WBNetworkManager.shared.unreadCount
        
        self.tabBar.items?[0].badgeValue = "\(count)"
            //count > 0 ? "\(count)" : nil
        
        //设置 APP bgNum , 从 iOS8.0后,要用户授权才可以显示
        UIApplication.shared.applicationIconBadgeNumber = count

    }
    
    @objc func userLogin(n: Notification){
        //print("用户登录通知\(n)")
        var deadline = DispatchTime.now()
        
        //判断 n.object 是否有值 -> token 过期 , 提示用户重新登录
        if n.object != nil {
            //渐变样式
            SVProgressHUD.setDefaultMaskType(.gradient)
            SVProgressHUD.showInfo(withStatus: "用户登录已超时,需要重新登录")
            
            deadline = DispatchTime.now() + 2
        }
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: {
            SVProgressHUD.setDefaultMaskType(.clear)
            //展现登录控制器, - 通常会和 UINavigationController 连用,方便返回
            let nav = UINavigationController(rootViewController:  WBOAuthController())
            self.present(nav, animated: true, completion: nil)

        })

    }
}

//MARK: - 设置界面
extension WBMainViewController{
    
    //撰写按钮
    func setupComposeBtn() -> () {
        tabBar.addSubview(composeBtn)
        
        //计算按钮宽度
        let count = CGFloat(childViewControllers.count)
        
        //向内缩进的宽度减少
        let w = tabBar.bounds.width / count
        
        //CGRectInset -- OC 正数向内缩进,负数向外拓展
        composeBtn.frame = tabBar.bounds.insetBy(dx: 2 * w, dy: 0)
        
        //按钮监听方法
        composeBtn.addTarget(self, action: #selector(composeStatus), for: .touchUpInside)
        
    }
    
    ///设置所有子控制器
    func setupChildControllers(){
        
        //数组 -> JSON : 序列化
        //let data = try! JSONSerialization.data(withJSONObject: arr, options: [])
        //(data as NSData).write(toFile: "/Users/mac/desktop/demo.json",atomically: true)
        
        // 0. 获取沙盒 json 路径
        let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let jsonPath = (docDir as NSString).appendingPathComponent("main.json")
        
        // 加载 data
        var data = NSData(contentsOfFile: jsonPath)
        
        // 判断 data 是否有内容，如果没有，说明本地沙盒没有文件
        if data == nil {
            // 从 Bundle 加载 data
            let path = Bundle.main.path(forResource: "main.json", ofType: nil)
            //从 plist 取出 data
            data = NSData(contentsOfFile: path!)
        }
        // 反序列化 : JSON -> 数组
        guard let array = try? JSONSerialization.jsonObject(with: data! as Data, options: []) as? [[String: AnyObject]]
            else {
                return
        }
        
        // 遍历数组，循环创建控制器数组
        var arrayM = [UIViewController]()
        for dict in array! {
            
            arrayM.append(controller(dict))
        }
        
        // 设置 tabBar 的子控制器
        viewControllers = arrayM
    }
    
    //MARK: - 使用字典创建一个子控制器
    // - parameter dict: 信息字典[claName,title,imageName,visitorInfo]
    // - 子控制器
    private func controller(_ dict: [String : AnyObject]) -> UIViewController {
        
        //1.取得字典内容
        guard let clsName = dict["clsName"] as? String,
            let title = dict["title"] as? String,
            let imageName = dict["imageName"] as? String,
            let cls = NSClassFromString(Bundle.main.namespace + "." + clsName) as? WBBaseController.Type,
            let visitorDict = dict["visitorInfo"] as? [String: String]
            else{
                
                return UIViewController()
        }
        
        //2.创建视图控制器 对象
        let vc = cls.init()
        vc.title = title
        
        //设置控制器的访客信息字典
        vc.visitorInfoDictionary = visitorDict
        
        //3.
        vc.tabBarItem.image = UIImage(named: "tabbar_" + imageName)
        vc.tabBarItem.selectedImage = UIImage(named: "tabbar_" + imageName + "_selected")?.withRenderingMode(.alwaysOriginal)
        
        //4.tabbar 标题字体(大小)
        vc.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.orange], for: .selected)
        //系统默认是12号字 ,修改字体大小,.normal
        //vc.tabBarItem.setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 12)], for: .normal)
        
        //实例化导航控制器的时候,会调用 push 的方法,rootVC压栈,但是并不增加 childControllers;
        let nav = WBNavigationController(rootViewController: vc)
        
        return nav
        
    }
    
}

