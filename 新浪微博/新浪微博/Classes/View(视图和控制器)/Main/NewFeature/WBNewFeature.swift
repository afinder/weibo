//
//  WBNewFeature.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/29.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit

//新特性视图 - 版本更新
class WBNewFeature: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.orange
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
