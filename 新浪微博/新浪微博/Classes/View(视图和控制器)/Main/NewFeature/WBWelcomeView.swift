//
//  WBWelcomeView.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/29.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit
import SDWebImage

//欢迎视图 - 没有新版本
class WBWelcomeView: UIView {
    
    @IBOutlet weak var iconView: UIImageView!

    @IBOutlet weak var tipLabel: UILabel!
    
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
     func welcomView() -> WBWelcomeView{
        
        let nib = UINib(nibName: "WBWelcomeView", bundle: nil)
        
        let v = nib.instantiate(withOwner: nil, options: nil)[0] as! WBWelcomeView
        
        //从 XIB 加载的视图默认 600 * 600
        v.frame = UIScreen.main.bounds
        
        return v
        
    }
    
    //required init?(coder aDecoder: NSCoder){
        //super.init(coder: aDecoder)
        
        //只是刚刚从 XIB 的二进制文件将视图数据加载完成
        //未和代码 连接建立关系,开发时,不要处理 UI
    //}
    
    override func awakeFromNib() {
        //1.url
        guard let urlIcon = WBNetworkManager.shared.userAccount.avatar_large,
            let url = URL(string: urlIcon) else {
            return
        }
        
        //2.设置头像
        iconView.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar_default_big"))
        
    }
    
    //自动布局系统更新完成的约束后,会自动调用此方法
    //override func layoutSubviews() {
        
    //}
    
    //视图被添加到 window 上,表示视图已经显示
    override func didMoveToWindow() {
        super.didMoveToWindow()
        
        //视图 是使用自动布局来设置的,只是设置了约束
        // - 当视图被添加到窗口上,根据父视图的大小,计算约束值,更新控件位置
        // - layoutIfNeeded 会直接按照当前的约束 直接更新 控件 位置
        // - 执行之后,控件所在位置,就是 XIB 中布局的位置
        self.layoutIfNeeded()
        bottomCons.constant = bounds.size.height - 200
        
        //如果控件 的 frame 还没有计算好,所有的约束会一起动画
        UIView.animate(
            withDuration: 2.0,
            delay: 0,
            usingSpringWithDamping: 0.7,
            initialSpringVelocity: 0,
            options: [],
            animations: {
            
                self.layoutIfNeeded()
        }, completion: { (_) in
            
            UIView.animate(withDuration: 1.0, animations: {
                self.tipLabel.alpha = 1
            })
        })
        
    }

}
