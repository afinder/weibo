//
//  WBDemoViewController.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/17.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit

class WBDemoViewController: WBBaseController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //设置标题
        title = "\(navigationController?.childViewControllers.count ?? 0)"
    }
    
    // MARK: - 监听方法
    //继续 push 新的控制器
    func showNext(){
        let vc = WBDemoViewController()
        navigationController?.pushViewController(vc, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


extension WBDemoViewController{
    
    override func setupTableView() {
        super.setupTableView()
        navItem.rightBarButtonItem = UIBarButtonItem(title: "下一个", fontSize: 14, target: self, action: #selector(showNext))
    }

}
