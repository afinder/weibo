//
//  WBVisitorView.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/19.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit

class WBVisitorView: UIView {
    
    //注册
    lazy var registerBtn: UIButton = UIButton.cz_textButton(
        "注册",
        fontSize: 16,
        normalColor: UIColor.orange,
        highlightedColor: UIColor.black,
        backgroundImageName: "common_button_white_disable")
    //登录
    lazy var loginBtn: UIButton = UIButton.cz_textButton(
        "登录",
        fontSize: 16,
        normalColor: UIColor.black,
        highlightedColor: UIColor.darkGray,
        backgroundImageName: "common_button_white_disable")

    //访客视图的信息字典[imageName / message]
    // if 首页 imageName == ""
    var visitorInfo: [String: String]?{
        didSet{
            //1.取字典信息
            guard let imageName = visitorInfo?["imageName"],
                let message = visitorInfo?["message"] else{
                    return
            }
            
            //2.消息
            tipLabel.text = message
            
            //3.设置图像,首页不需要
            if imageName == "" {
                startAnimation()
                return
            }
            
            iconView.image = UIImage(named: imageName)
            
            //其他控制器的视图,不需要显示小房子
            houseIconView.isHidden = true
            maskIconView.isHidden = true
        }
    }
    
    
    //MARK: - 构造属性
    override init(frame: CGRect) {
        ///Property 'self.visitorInfo' not initialized at super.init call
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //旋转图标动画
    private func startAnimation() {
        let anim = CABasicAnimation(keyPath: "transform.rotation")
        anim.toValue = 2 * M_PI
        anim.repeatCount = MAXFLOAT
        anim.duration = 15
        
        //动画完成不删除,如果 iconView 被释放,动画会一起销毁
        anim.isRemovedOnCompletion = false
        //将动画添加到图层,在设置连续动画中有用
        iconView.layer.add(anim, forKey: nil)
    }

    
    //MARK: - 私有控件
    //懒加载属性 : 只有调用 UIKit 控件的指定构造函数,其他需要使用类型
    lazy var iconView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    //遮罩
    lazy var maskIconView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
    lazy var houseIconView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
    lazy var tipLabel: UILabel = UILabel.cz_label(
        withText: "关注一些人,会看有什么惊喜,关注一些人,会看有什么惊喜,关注一些人,会看有什么惊喜,关注一些人,会看有什么惊喜,",
        fontSize: 14, color: UIColor.darkGray)


}


//MARK: 设置界面
extension WBVisitorView{
    
    func setupUI() -> () {
        backgroundColor = UIColor.cz_color(withHex: 0xEDEDED)
        
        //1.
        addSubview(iconView)
        addSubview(maskIconView)
        addSubview(houseIconView)
        addSubview(tipLabel)
        addSubview(registerBtn)
        addSubview(loginBtn)
        tipLabel.textAlignment = .center
        
        //2.(取消 autoresizing) 与 autoLayout 不能共存
        for v in subviews {
            v.translatesAutoresizingMaskIntoConstraints = false
        }
        
        //3.自动布局
        let margin: CGFloat = 20.0
        //1> 图像视图
        addConstraint(NSLayoutConstraint(item: iconView,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerX,
                                         multiplier: 1.0,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: iconView,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerY,
                                         multiplier: 1.0,
                                         constant: -60))
        //2> 小房子
        addConstraint(NSLayoutConstraint(item: houseIconView,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerX,
                                         multiplier: 1.0,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: houseIconView,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: iconView,
                                         attribute: .centerY,
                                         multiplier: 1.0,
                                         constant: 0 ))
        
        //3> 提示标签
        addConstraint(NSLayoutConstraint(item: tipLabel,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: iconView,
                                         attribute: .centerX,
                                         multiplier: 1.0,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: tipLabel,
                                         attribute: .top,
                                         relatedBy: .equal,
                                         toItem: iconView,
                                         attribute: .bottom,
                                         multiplier: 1.0,
                                         constant: margin))
        addConstraint(NSLayoutConstraint(item: tipLabel,
                                         attribute: .width,
                                         relatedBy: .equal,
                                         toItem: nil,
                                         attribute: .notAnAttribute,
                                         multiplier: 1.0,
                                         constant: 236))
        //注册
        addConstraint(NSLayoutConstraint(item: registerBtn,
                                         attribute: .left,
                                         relatedBy: .equal,
                                         toItem: tipLabel,
                                         attribute: .left,
                                         multiplier: 1.0,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: registerBtn,
                                         attribute: .top,
                                         relatedBy: .equal,
                                         toItem: tipLabel,
                                         attribute: .bottom,
                                         multiplier: 1.0,
                                         constant: margin))
        addConstraint(NSLayoutConstraint(item: registerBtn,
                                         attribute: .width,
                                         relatedBy: .equal,
                                         toItem: nil,
                                         attribute: .notAnAttribute,
                                         multiplier: 1.0,
                                         constant: 100))
        //根据背景默认高度
        
        //5登录
        addConstraint(NSLayoutConstraint(item:  loginBtn,
                                         attribute: .right,
                                         relatedBy: .equal,
                                         toItem: tipLabel,
                                         attribute: .right,
                                         multiplier: 1.0,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: loginBtn,
                                         attribute: .top,
                                         relatedBy: .equal,
                                         toItem: tipLabel,
                                         attribute: .bottom,
                                         multiplier: 1.0,
                                         constant: margin))
        addConstraint(NSLayoutConstraint(item: loginBtn,
                                         attribute: .width,
                                         relatedBy: .equal,
                                         toItem: registerBtn,
                                         attribute: .width,
                                         multiplier: 1.0,
                                         constant: 0))
        //6
        //views: 定义 VFL 中的控件名称和释迦名称映射关系
        let viewDict: [String : AnyObject] = ["maskIconView": maskIconView,
                        "registerBtn":registerBtn]
        let metrics = ["spacing": 20]
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[maskIconView]-0-|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: viewDict))
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[maskIconView]-(spacing)-[registerBtn]",
            options: [],
            metrics: metrics,
            views: viewDict))
        
    }
}
