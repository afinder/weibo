//
//  WBBaseController.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/16.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit

//注:
//OC不支持多继承,使用协议替代
//extension 可以把函数 按照功能分类管理, 不能有属性, 不能重写父类方法

//所有主控制器的基类控制器
class WBBaseController: UIViewController {

//MARK: - 定义
    //登录状态
    //var userLogon = true
    var visitorInfoDictionary: [String: String]?
    
    ///表格视图 - 如果用户没有登录,就不创建
    var tableView: UITableView?
    var refreshControl: UIRefreshControl?
    //上拉刷新标记
    var isPullup = false
    
    ///自定义导航条
    lazy var navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.cz_screenWidth(), height: 64))
    
    //自定义的导航 条目 - 以后设置导航栏内容,统一使用 navItem
    lazy var navItem = UINavigationItem()

//MARK:- 重写
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        WBNetworkManager.shared.userLogon ? loadData() : ()
        
        //注册通知
        NotificationCenter.default.addObserver(self, selector: #selector(loginSuccess), name: NSNotification.Name(rawValue: WBUserLoginSuccessedNotification), object: nil)
    }
    
    deinit {
        //注销通知
        NotificationCenter.default.removeObserver(self)
    }
    
    //重写 title 的 didset
    override var title: String?{
        didSet{
            navItem.title = title
        }
    }
    
    func loadData() -> () {
        //子类不实现任何方法,也能停止刷新
        refreshControl?.endRefreshing()
    }
    
    private func setupUI() -> () {
        //取消自动缩进 - 如果隐藏了导航栏,会缩进20个点
        automaticallyAdjustsScrollViewInsets = false
        
        setupNavigationBar()
        
        WBNetworkManager.shared.userLogon ? setupTableView() : setupVisitorView()
        view.backgroundColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


//MARK: - 访客视图监听方法
//代理的核心是 解耦 和 视图被多个控制器使用
extension WBBaseController {
    
    //登录成功处理
    @objc func loginSuccess(n: Notification) {
        print("登录成功")
        //登录前左为注册,右为登录
        navItem.leftBarButtonItem = nil
        navItem.rightBarButtonItem = nil
        
        //更新 UI
        //重新设置 View
        //在访问 view 的 getter 时,当 view == nil,会调用 loadView -> viewDidLoad
        view = nil
        
        //注销通知 -> 重新执行 viewDidLoad 会再次注册,避免通知被重复注册
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func login(){
        //发送通知
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: WBUserShouldLoginNotification), object: nil)
    }
    
    @objc func register(){
        print("用户注册")
    }
}

//MARK: - 设置界面
extension WBBaseController{
    
    func setupNavigationBar() -> () {
        
        view.addSubview(navigationBar)
        navigationBar.items = [navItem]
        navigationBar.barTintColor = UIColor.cz_color(withHex: 0xf6f6f6)
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.darkGray]
        //系统按钮的文字渲染颜色
        navigationBar.tintColor = UIColor.orange
    }
    
    //MARK: - 访客视图
    func setupVisitorView(){
        
        let visitorView = WBVisitorView(frame: view.bounds)
        view.insertSubview(visitorView, belowSubview: navigationBar)
        
        //1.设置访客信息
        visitorView.visitorInfo = visitorInfoDictionary
        
        //2.添加访客视图 按钮的监听
        visitorView.loginBtn.addTarget(self, action: #selector(login), for: .touchUpInside)
        visitorView.registerBtn.addTarget(self, action: #selector(register), for: .touchUpInside)
        
        //3.设置导航条按钮
        navItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: .plain, target: self, action: #selector(register))
        navItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: .plain, target: self, action: #selector(login))
    }
    
    //登录之后;子类重写此方法,因为子类不需要关心登录之前的逻辑
    func setupTableView() -> () {
        tableView = UITableView(frame: view.bounds, style: .plain)
        view.insertSubview(tableView!, belowSubview: navigationBar)
        
        //设置数据源&代理 -> 目的: 子类直接实现数据源方法
        tableView?.delegate = self
        tableView?.dataSource = self
        
        //内容缩进
        tableView?.contentInset = UIEdgeInsets(top: navigationBar.bounds.height, left: 0, bottom: tabBarController?.tabBar.bounds.height ?? 0, right: 0)
        
        //修改指示器的缩进 - 强行解包是为了拿到一个必有的 inset
        tableView?.scrollIndicatorInsets = tableView!.contentInset
        
        //设置刷新控件
        refreshControl = UIRefreshControl()
        tableView?.addSubview(refreshControl!)
        refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
    }
    
    
}

//MARK: - UITableViewDataSource UITableViewDelegate
extension WBBaseController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    //子类不需要 super
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //只是保证没有语法错误
        return UITableViewCell()
    }
    
    //在显示最后一行的时候,做上拉刷新
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //1.判断 indexPath 是否是最后一组,最后一行
        let row = indexPath.row
        let section = tableView.numberOfSections - 1
        
        if row < 0 || section < 0 {
            return
        }
        
        let count = tableView.numberOfRows(inSection: section)
        
        //如果是最后一行 && 没有开始上拉刷新
        if row == (count - 1) && !isPullup {
            print("上拉刷新")
            isPullup = true
            loadData()
        }        
    }
}
