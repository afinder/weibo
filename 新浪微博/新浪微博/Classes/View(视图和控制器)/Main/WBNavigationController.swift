//
//  WBNavigationController.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/16.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit

class WBNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //隐藏默认的 NavigationBar
        navigationBar.isHidden = true
        
    }
    //viewController 是被 push 的控制器,设置他的左侧按钮作为返回按钮
    //重写 push -- 所有的 push 都会调用此方法
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        //如果不是栈底控制器隐藏, 根控制器不隐藏
        if childViewControllers.count > 0{
            
            viewController.hidesBottomBarWhenPushed = true
            
            if let vc = viewController as? WBBaseController {
                
                var left = "返回"
                
                //判断控制器的级数
                if childViewControllers.count == 1 {
                    //title 显示首页的标题
                    left = (childViewControllers.first?.title ?? "返回")!
                }
                //取出自定义的 navItem,设置左侧按钮为返回按钮
                vc.navItem.leftBarButtonItem = UIBarButtonItem(title: left, target:  self, action: #selector(popToParent),isBack: true)
            }
            
        }
        super.pushViewController(viewController, animated: true)
    }
    
    ///pop 上一级控制器
    func popToParent() -> () {
        popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}
