//
//  Bundle+Extentions.swift
//  反射机制
//
//  Created by 郭鹏飞 on 2016/11/15.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import Foundation

extension Bundle{
    
    ///返回命名空间字符串 (对象函数)
//    func nameSpace() -> String {
//        
//        // 第二个? 前面用?强转也用?
//        //return Bundle.main.infoDictionary?["CFBundleName"] as? String ?? ""
//        return infoDictionary?["CFBundleName"] as? String ?? ""
//    }
    
    //计算型属性 类似于函数,没有参数,只有返回值
    //利用函数调用
    var namespace: String {
        return infoDictionary?["CFBundleName"] as? String ?? ""
    }
}
