//
//  WBNetworkManager.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/22.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit
import AFNetworking

//SWift 的枚举支持任意数据类型
//Switch /enum 在 OC 中都只支持整数
enum WBHTTPMethod {
    case GET
    case POST
}

//网络管理工具
class WBNetworkManager: AFHTTPSessionManager {
    
    //静态区/常量/闭包/
    //在第一访问时执行闭包,执行闭包,并且将结果保存在 shared 常量中
    static let shared: WBNetworkManager = {
        
        //实例化对象
       let instance = WBNetworkManager()
        
        //设置响应反序列化 支持的数据格式
        instance.responseSerializer.acceptableContentTypes?.insert("text/plain")
        
        //返回对象
        return instance
    }()
    
    //微博账户 懒加载属性
    lazy var userAccount = WBUserAccount()
    
    //用户登录标记[计算型属性]
    var userLogon: Bool{
        return userAccount.access_token != nil
    }

    //专门负责 拼接 token 的网络请求方法
    func tokenRequest(method: WBHTTPMethod = .GET,URLString: String,parameters: [String : AnyObject]?,completion: @escaping (_ json: AnyObject?,_ isSuccess: Bool)->() ) -> () {
        
        //处理 token 字典
        //判断 token 是否为 nil,程序执行过程中,一般 token 不会为 nil
        guard let token = userAccount.access_token else {
            completion(nil,false)
            
            //发送通知,提醒登录
            print("没有 Token, 需要登录")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: WBUserShouldLoginNotification), object: nil)
            return
        }
        
        //判断参数字典是否存在 ,如果为 nil,应该新建字典
        var parameters = parameters
        
        if parameters == nil{
            //实例化字典
            parameters = [String: AnyObject]()
        }
        
        //设置参数字典
        parameters!["access_token"] = token as AnyObject?
        
        //调用 request发起真正的网络请求方法
        request(URLString: URLString, parameters: parameters, completion: completion)
    }
    
    
    //使用一个函数封装 AFN 的 get/post 请求
    
    /// 专门负责拼接 token 的网络请求方法
    ///
    /// - parameter method:     GET / POST
    /// - parameter URLString:  URLString
    /// - parameter parameters: 参数字典
    /// - parameter name:       上传文件使用的字段名，默认为 nil，不上传文件
    /// - parameter data:       上传文件的二进制数据，默认为 nil，不上传文件
    /// - parameter completion: 完成回调
    
    func request(method: WBHTTPMethod = .GET,URLString: String,parameters: [String : AnyObject]?,completion: @escaping (_ json: AnyObject?,_ isSuccess: Bool)->() ) -> () {
        
        //成功回调
        let success = { (task: URLSessionDataTask, json: Any?)->() in
            
            completion(json as AnyObject?,true)
        }
        
        //失败回调
        let failure = { (task: URLSessionDataTask?,error: Error)->() in
            //针对403处理用户 token 过期 //转换到子类
            if (task?.response as? HTTPURLResponse)?.statusCode == 403{
                
                //发送通知(本方法不知道被谁调用,谁接收到通知,谁处理)
                //提示登录
                print("token 过期了")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: WBUserShouldLoginNotification), object: "bad token")
            }
            //不适合返回
            print("网络请求错误\(error)")
            completion(nil ,false)
        }
        
        if method == .GET {
            
            get(URLString, parameters: parameters, progress: nil, success: success, failure: failure)
        }else{
            
            post(URLString, parameters: parameters, progress: nil, success: success, failure: failure)
        }
        
    }
}
