//
//  WBUserAccount.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/26.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import UIKit

let accountFile: NSString = "useraccount.json"

class WBUserAccount: NSObject {
    
    var access_token: String?
    
    var uid: String?
    
    //token 的生命周期
    //过期日期 
    // - 开发者5年 
    // - 使用者 3天
    var expires_in: TimeInterval = 0 {
        didSet {
            expiresDate = Date(timeIntervalSinceNow: expires_in)
        }
    }
    
    //过期日期
    var expiresDate: Date?
    
    //昵称
    var screen_name: String?
    //用户大图头像
    var avatar_large: String?
    
    override var description: String{
        return yy_modelDescription()
    }
    
    //构造函数
    override init() {
        super.init()
        
        //从磁盘加载保存的文件 -> 字典
        guard let path = accountFile.cz_appendTempDir(),
            let data = NSData(contentsOfFile: path),
        let dict = try? JSONSerialization.jsonObject(with: data as Data, options: []) as? [String : AnyObject] else{
                return
        }

        //使用字典设置属性值
        // *** 用户是否登录的关键
        yy_modelSet(with: dict ?? [:])
        
        //3.判断 token 过期
        //判断过期日期
//        if expiresDate?.compare(Date()) == .orderedDescending {
//            print("账户过期")
//            //清空
//            access_token = nil
//            uid = nil
//            
//            //删除账户文件
//            try? FileManager.default.removeItem(atPath: path)
//        }
       // print("账户未过期\(self)")
    }
    
    /**
     1.偏好设置(小)
     2.沙盒 - 归档/plist/ 'json'
     3.数据库 (FMDB/CoreData)
     4.钥匙串访问(小/自动加密 - 需要使用框架 SSKeyChain)
    */
    func savaAccount() -> () {
        
        //1.模型转字典 
        var dict = (self.yy_modelToJSONObject() as? [String : AnyObject]) ?? [:]
        
        dict.removeValue(forKey: "expires_in")
        //2.删除 expires_in 的值
        
        //2.字典序列化
        guard let data = try? JSONSerialization.data(withJSONObject: dict, options: []),
            let filePath = accountFile.cz_appendTempDir() else {
                
                return
        }
        //3.写入磁盘
        (data as NSData).write(toFile: filePath, atomically: true)
        
        //print("用户账户保存成功\(filePath)") 
    }
    
}
