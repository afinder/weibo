//
//  WBStatusListViewModel.swift
//  新浪微博
//
//  Created by 郭鹏飞 on 2016/11/22.
//  Copyright © 2016年 郭鹏飞. All rights reserved.
//

import Foundation

//微博数据列表视图模型
/**父类的选择
 - 如果类需要使用"KVC"或者字典转模型框架设置对象值,类就需要继承自 NSObject
 - 如果类只是包装一些代码逻辑(写了一些函数),可以不用任何父类,好处: 更加轻量级
 - OC 写,一律继承自 NSObject
 */

//上拉刷新最大尝试次数
private let maxPullupTryTimes = 3

//微博数据处理
class WBStatusListViewModel {
    
    //微博模型数组
    // 1.字典转模型
    // 2.下拉/上拉刷新数据处理
    lazy var statusList = [WBStatus]()
    
    //上拉刷新错误次数
    private var pullupErrorTimes = 0
    
    //完成回调[网络请求是否成功]
    // - 是否上拉刷新标记
    // - 完成回调[网络请求是否成功,能否继续上拉刷新]
    func loadStatus( _ pullup: Bool, completion: @escaping (_ isSuccess: Bool, _ shouldRefresh: Bool)->()) {
        
        // 上拉刷新  刷新错误
        if pullup && (pullupErrorTimes > maxPullupTryTimes) {
            
            completion(true, false)
            
            return
        }
        
        //since_id 下拉, 取出数组中第一条微博的 id 最小,越新 id 越大
        let since_id = pullup ? 0 : (statusList.first?.id ?? 0)
        //上拉刷新 , 取出数组的最后一条微博的 id
        let max_id = !pullup ? 0 : (statusList.last?.id ?? 0)
        

        WBNetworkManager.shared.statusList(since_id: since_id,max_id: max_id){ (list, isSuccess) in
            
            //1.字典转模型
            //array 是结构体
            //数据放入模型的过程
            guard let array = NSArray.yy_modelArray(with: WBStatus.self, json: list ?? []) as?[WBStatus] else{
                //字典转模型失败,没有数据
                completion(isSuccess,false)
                return
            }
            print("刷新到\(array.count)条数据")
            
            //2.拼接数据

            if pullup{
                self.statusList += array
            }else{
                //下拉刷新,应该将结果数组,拼接在数组前面
                self.statusList = array + self.statusList
            }
            
            //3.判断上拉刷新的数据量
            if pullup && array.count == 0{
                
                self.pullupErrorTimes += 1
                completion(isSuccess, false)
                
            }else{
                //3.完成回调
                completion(isSuccess,true)

            }
            
            
        }
    }
}
